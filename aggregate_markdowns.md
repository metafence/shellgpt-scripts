# aggregate_markdowns.sh

This script is used to generate or update a README.md file by aggregating multiple Markdown files. It takes in command-line options to specify the mode of operation and the directory containing the Markdown files.

## Requirements

- [ShellGPT](https://github.com/TheR1D/shell_gpt?ref=firstfinger.in) must be installed and functional 
- API quotas for the `openai API ` should be sufficient.

## Usage

```
aggregate_markdowns.sh [-m|--mode MODE] [-d|--directory DIRECTORY]
```

Options:
- `-m, --mode`: Mode of operation. Can be 'create', 'update', or 'lenient'. Default is 'create'.
- `-d, --directory`: Directory containing the Markdown files. Default is the current directory.

## Functionality

The script performs the following steps:

1. Initializes variables and sets default values.
2. Displays help if no arguments are provided.
3. Parses command-line options to set the mode and directory.
4. Checks if the specified directory exists.
5. Checks if the README.md file exists based on the mode.
6. Initializes the shellGPT session and sends an initial query.
7. Iterates over each Markdown file in the directory and sends its content to the shellGPT session.
8. Composes the final query based on the mode and existing README.md file.
9. Sends the final query to the shellGPT session and saves the response to the README.md file.

## Potential Error Sources

- If no arguments are provided, the script displays the help message and exits.
- If the specified directory does not exist, an error message is displayed and the script exits.
- If the README.md file already exists in 'create' mode, an error message is displayed and the script exits.
- If the README.md file does not exist in 'update' mode, an error message is displayed and the script exits.

## Proposed Changes

- The existing comments in the script are accurate and clear. No changes are needed.

This documentation was generated using shellGPT and ChatGPT.
