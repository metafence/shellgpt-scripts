# ./document_code.sh

This script generates Markdown documentation for the given files using the `shellGPT` - tool. The file is sent for analysis to chatGPT, which then returns a markdown file describing the function of the script.

## Usage

General

```bash
./document_code.sh [--recursive,-r ] [file1.sh file2.sh ...]
```
Document all source-like files in current directory

```bash
./document_code.sh .
```
Document all source-like files in current directory and below

```bash
./document_code.sh -r .
```

Document all sh-Files in current directory

```bash
./document_code.sh *.sh
```

## Requirements

- [ShellGPT](https://github.com/TheR1D/shell_gpt?ref=firstfinger.in) must be installed and functional 
- API quotas for the `openai API ` should be sufficient.

## Functionality

The script takes one or more source code or script files as input and generates Markdown documentation for each file. 
The documentation is created using the `shellGPT` tool, asking chatGPT. The generated documentation is saved in the 
same directory as the input file with the extension `.md`.
