#!/bin/bash

# MIT License
#
# Copyright (c) 2023 metafence oü, Sepapaja tn 6, 15551 Tallinn, Harju Maakond, Estonia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Documentation
#
# Name: fix_code.sh
# Description: This script sends text files to sgpt for code improvement and security checks.
# Usage: ./fix_code.sh [--savedir DIR] [--recurse] [file1 file2 ...]
# Options:
#   --savedir, -s DIR  Directory to save original files before overwriting
#   --recurse          Recursively search directories for files to process

# Initialize variables
SAVEDIR=""
RECURSE=false
FILES=()  # Initialize FILES as an empty array

# Function to display help
display_help() {
  echo "Usage: $0 [--savedir DIR] [--recurse] [file1 file2 ...]"
  echo "Send text files to sgpt for code improvement and security checks."
}

# Parse command-line options
while [[ "$#" -gt 0 ]]; do
  case $1 in
    --savedir|-s) SAVEDIR="$2"; shift;;
    --recurse) RECURSE=true;;
    *) FILES+=("$1");;
  esac
  shift
done

# Function to process a file
process_file() {
  local file=$1
  local random_number=$((RANDOM % 10000))  # Generate a random number
  local session_id="code_improvement_session_$random_number"  # Append random number to session ID
  
  # Send the file to sgpt for processing
  improved_code=$(sgpt --no-cache --chat "$session_id" <<- EOM
  Improve the following code by eliminating security vulnerabilities, 
  improving error-prone areas, enhancing readability, adding or updating 
  header and inline documentation, correcting any formatting issues and 
  adhering to the recommended maximum line length for the language. Do not 
  enclose the code in Markdown code blocks. Append a changelog at the end of 
  the file, using the block comment syntax appropriate for the language of the
  code. If a changelog section already exists, use the same section and 
  formatting to log your changes:
  $(cat $file)
EOM
  )
  
  # Save the improved code back to the file
  if [[ -n "$improved_code" ]]; then
    if [[ -n "$SAVEDIR" ]]; then
      mv "$file" "$SAVEDIR"
    fi
    echo "$improved_code" > "$file"
  else
    echo "Failed to improve $file."
  fi
}

# Function to process a directory recursively
process_directory() {
  find "$1" -type f -exec bash -c 'process_file "$0"' {} \;
}

# Main loop to process files
for file in "${FILES[@]}"; do
  if [[ -f "$file" ]]; then
    process_file "$file"
  elif [[ -d "$file" && "$RECURSE" == "true" ]]; then
    process_directory "$file"
  else
    echo "Skipping $file (not a recognized file or directory)"
  fi
done
