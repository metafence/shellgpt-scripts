#!/bin/bash

# MIT License
#
# Copyright (c) 2023 metafence oü, Sepapaja tn 6, 15551 Tallinn, Harju Maakond, Estonia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Documentation
#
# Name: document_code.sh
# Description: This script generates Markdown documentation for the given code files.
# This script is created using ChatGPT.
# Usage: ./document_code.sh [-r|--recursive] [file1 file2 ...]
# Requirements: shellGPT must be installed and functional (https://github.com/TheR1D/shell_gpt?ref=firstfinger.in))
# API quotas for openai API should be sufficient.

# Initialize variables
RECURSIVE=false

# Mapping of file extensions to MIME types
declare -A file_types=(
  ["sh"]="text/x-shellscript"
  ["py"]="text/x-python"
  ["c"]="text/x-c"
  ["cpp"]="text/x-c++"
  ["java"]="text/x-java"
  ["js"]="text/x-javascript"
  ["cs"]="text/x-csharp"
)

# Function to display help
display_help() {
  echo "Usage: $0 [-r|--recursive] [file1 file2 ...]"
  echo "Generate Markdown documentation for the given code files."
  echo "Options:"
  echo "  -r, --recursive  Process files recursively in directories."
}

# Function to process a file
process_file() {
  local file=$1
  local dir=$(dirname "$file")
  local base=$(basename "$file")
  local output_path="$dir/${base%.*}.md"

  # Skip Markdown files
  if [[ "${file##*.}" == "md" ]]; then
    echo "Skipping Markdown file $file."
    return
  fi

  # Check if Markdown file already exists
  if [[ -f "$output_path" ]]; then
    echo "Warning: Markdown file $output_path already exists. Skipping $file."
    return
  fi

  echo "Processing $file..."
  
  # Determine file type
  local extension="${file##*.}"
  local mime_type="${file_types[$extension]}"

  # Using a Here Document for the sgpt prompt
  local random_id=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)
  documentation=$(sgpt --chat "doc_$random_id" <<- EOM
    Create documentation for the following code file in Markdown and English. 
    Include a brief summary of the file's functionality, usage, and potential error sources. 
    Also, analyze any existing documentation, for example in the comments of the script, 
    and verify their accuracy and use of language. If they are wrong or a better wording is available, 
    propose changes at the bottom of the documentation in a dedicated section. Add a note like 'This documentation  was generated using shellGPT and ChatGPT'.
    Max line length 80 characters for the whole created document.
    Fine Name: $file
    Content: 
    $(cat $file)
EOM
)
  
  # Only write to file if documentation is generated
  if [[ -n "$documentation" ]]; then
    echo "$documentation" > "$output_path"
    echo "Documentation generated at $output_path"
  else
    echo "Failed to generate documentation for $file."
  fi
}

# Check if any filenames were passed as parameters
if [ "$#" -eq 0 ]; then
  display_help
  exit 1
fi

# Loop through all files passed as arguments
for file in "$@"; do
  extension="${file##*.}"
  mime_type="${file_types[$extension]}"

  # Check if the file is a recognized code file
  if [[ -n "$mime_type" ]]; then
    process_file "$file"
  else
    echo "Skipping $file (not a recognized code file)"
  fi
done