#!/bin/bash

# MIT License
#
# Copyright (c) 2023 metafence oü, Sepapaja tn 6, 15551 Tallinn, Harju Maakond, Estonia
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Constants

QUERY_INIT="I will sequentially provide you with the contents of multiple Markdown files along with their names and paths. After all files have been provided, I will give you further instructions on how to proceed. You will not provide any intermediate results, only respond with 'ok', starting with the response to this request."

QUERY_UPDATE="Update the attached README.md based on the contents of the previously provided files."

QUERY_CREATE="Create a README.md based on the contents of the previously provided files."

QUERY_CONTENT="Find the common theme and probable target audience for the received data. Create a meaningful summary and title for the final result. Ensure that a correct table of contents exists for the first three levels. Make sure that the previously provided Markdown files are listed in a table with the columns 'Name', 'File', and 'Content', where the Name contains a local reference and the File contains a clickable relative reference to the corresponding file. Finally, the 'Content' column should contain a short (1 line) description of the content/goal or function of each file compiled by you. Standardize directory levels and adjust the language usage to the presumed target audience."
    

# Initialize variables
MODE="create"
DIRECTORY="."
README_PATH="./README.md"
SESSION_ID="readme_gen_${RANDOM}"



# Function to display help
display_help() {
  echo "Usage: $0 [-m|--mode MODE] [-d|--directory DIRECTORY]"
  echo "Generate or update a README.md file by aggregating Markdown files."
  echo "Options:"
  echo "  -m, --mode        Mode of operation: 'create', 'update', or 'lenient'"
  echo "  -d, --directory   Directory containing Markdown files"
}

# Check if no arguments were provided
if [[ "$#" -eq 0 ]]; then
  display_help
  exit 1
fi


# Parse command-line options
while [[ "$#" -gt 0 ]]; do
  case $1 in
    -m|--mode) MODE="$2"; shift;;
    -d|--directory) DIRECTORY="$2"; shift;;
    *) 
      if [[ -d "$1" ]]; then
        DIRECTORY="$1"
      else
        README_PATH="$1"
      fi
      ;;
  esac
  shift
done

# Check directory
if [[ -d "$DIRECTORY" ]]; then
  cd "$DIRECTORY"
else
  echo "This is either no directory or does not exist: $DIRECTORY"
  exit 1
fi

# Check README path
README_PATH=$(readlink -f $README_PATH)
if [[ -f "$README_PATH" ]]; then
    [[ "$MODE" == "create" ]] && ERROR="file $README_PATH already exists"
else
    [[ "$MODE" == "update" ]] && ERROR="file $README_PATH does not exists"
fi
[[ "$ERROR" != "" ]] && echo "$ERROR" && exit 1

# Init shellGPT
sgpt --chat "$SESSION_ID" "$QUERY_INIT"

# loop over markdown files
for md_file in *.md; do      
    echo Sending $DIRECTORY/$md_file ...
    sgpt --chat "$SESSION_ID" "
File Name: $DIRECTORY/$md_file
File Content: 
$(cat $md_file)
"
done

if [[ -f $README_PATH ]]; then

    # compose update query
    FINAL_QUERY="
${QUERY_UPDATE}
${QUERY_CONTENT}

Vorhandene README.md:
$(cat $README_PATH)
"
else
    # Compose create query
    FINAL_QUERY="
${QUERY_CREATE}
${QUERY_CONTENT}"
fi

echo FINAL_QUERY=$FINAL_QUERY
    
# Send final query
# echo "sgpt --chat \"$SESSION_ID\" \"$FINAL_QUERY\" > \"$README_PATH\"" 
sgpt --chat "$SESSION_ID" "$FINAL_QUERY" | tee "$README_PATH" 
        
