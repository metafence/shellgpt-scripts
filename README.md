# Some handy ShellGPT Scripts


Scripts to simplify and automate documentation and pre-audit tasks, making them suitable for developers, system administrators, and anyone working with shell scripting on a Linux/Ubuntu system.

Please refer to the individual script files for detailed usage instructions and additional information.

## Requirements

- [ShellGPT](https://github.com/TheR1D/shell_gpt?ref=firstfinger.in) must be installed and functional 
- API quotas for the `openai API ` should be sufficient.

## aggregate_markdowns.sh
- A script to generate or update a README.md file by aggregating multiple Markdown files from a directory using shellGPT.
- **Code**: [aggregate_markdowns.sh](aggregate_markdowns.sh)
- **Documentation**: [aggregate_markdowns.md](aggregate_markdowns.md)

## document_code.sh
- A script to generate Markdown documentation for source code files using shellGPT.
- **Code**: [document_code.sh](document_code.sh)
- **Documentation**: [document_code.md](document_code.md)

## fix_code.sh
- A script to send source code files to chatGPT using shellGPT, for code improvement and security checks.
- **Code**: [fix_code.sh](fix_code.sh)
- **Documentation**: [fix_code.md](fix_code.md)

## MIT License

```plaintext
MIT License
Copyright (c) 2023 metafence oü, Sepapaja tn 6, 15551 Tallinn, Harju Maakond,
Estonia
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```