# fix_code.sh

A script used to send text files to chatGPT for code improvement and security checks. It allows for the option to specify a directory to save the original files before overwriting them, and also supports recursive searching of directories for files to process.

## Requirements

- [ShellGPT](https://github.com/TheR1D/shell_gpt?ref=firstfinger.in) must be installed and functional 
- API quotas for the `openai API ` should be sufficient.

## Usage

```
./fix_code.sh [--savedir DIR] [--recurse] [file1 file2 ...]
```

Options:
- `--savedir, -s DIR`: Directory to save original files before overwriting.
- `--recurse`: Recursively search directories for files to process.

This documentation was generated using ShellGPT and ChatGPT.
